package com.itapril.service.impl;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;

import com.itapril.dao.IUserDao;
import com.itapril.model.User;
import com.itapril.service.IUserService;

/**
 * Created by itapril on 2017/12/19 8:41.
 */
@Service("userService")
public class UserServiceImpl implements IUserService {
    @Resource
    private IUserDao userDao;

    public User getUserById(int userId) {
        // TODO Auto-generated method stub
        return this.userDao.selectByPrimaryKey(userId);
    }
}
