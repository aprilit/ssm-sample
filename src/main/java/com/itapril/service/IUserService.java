package com.itapril.service;

import com.itapril.model.User;

/**
 * Created by itapril on 2017/12/19 8:32.
 */
public interface IUserService {
    public User getUserById(int userId);
}
