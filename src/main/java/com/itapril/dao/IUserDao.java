package com.itapril.dao;

import com.itapril.model.User;

/**
 * Created by itapril on 2017/12/19 8:33.
 */
public interface IUserDao {

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

}
