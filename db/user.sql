/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50534
Source Host           : 127.0.0.1:3306
Source Database       : itapril

Target Server Type    : MYSQL
Target Server Version : 50534
File Encoding         : 65001

Date: 2017-12-19 09:44:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(250) NOT NULL AUTO_INCREMENT,
  `user_name` bigint(255) DEFAULT NULL COMMENT '用户名',
  `password` bigint(255) DEFAULT NULL COMMENT '密码',
  `age` varchar(255) DEFAULT NULL COMMENT '虚拟机名称',
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', '1', '1', '2017-12-19 09:21:55', '2017-12-19 09:21:57');
